Hallo,
der neue Uebungszettel ist online.
Da man Netzwerke eigentlich nur an einem Rechner ueben kann, auf dem man Rootrechte hat, mache ich allen, die alle Uebungszettel bis einschliesslich Zettel 05 bearbeitet haben, folgendes Angebot:
Sie koennen sich einen aelteren lautlosen, kleinen ATOM-Rechner (ohne Festplatte) bei mir abholen.
- ATOM
- mind. 1 GB Hauptspeicher
- Tastatur (ca. 15-19 Jahre alt)
- Maus (ca. 15-19 Jahre alt)
- Monitor VGA (1280x1024 LED  (ca. 15-19 Jahre alt)
Was Sie mitbringen muessen, ist ein USB-Stick (moeglichst boot-faehig mit LINUX, wenn nicht, erstellen wir einen)



mfG Stefan Koospal Tel : +49 551 3927759
Software,Multimedia&Systeme Twitter: Zahlenzauberer
Mathematisches Institut Mobil : +49 160 8404155
Bunsenstr. 3-5 E-mail: stefan.koospal@mathematik.uni-goettingen.de
37073 Goettingen WWW : http://www.uni-math.gwdg.de/koospal


